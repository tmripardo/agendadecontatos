var AWS = require('aws-sdk');



function createBucketConnection(){
    var s3 = new AWS.S3();

    var bucketParams = {
        Bucket : "contatos-agenda"
    };

    s3.headBucket(bucketParams, function(err, data){
        if(err){
            console.log('Error connecting to Bucket');
        }
        else console.log("Connection established with bucket");
    });

    return s3;
}

module.exports = function(){
    return createBucketConnection;
}

