var AWS = require('aws-sdk');


function createDynamoConnection(){

    var dynamo = AWS.DynamoDB({region: us-west-2});

    var tableParams = {
        TableName: "contatos"
    };

    dynamo.describeTable(tableParams, function(err){
        if(err) console.log("Error connecting to Dynamo");
        else("Connection established with Dynamo");
    });

    return dynamo;
}

module.exports = function(){
    return createDynamoConnection;
}

