module.exports = function(app){
    
    app.get('/deletar', function(req, res){
        
        res.render("pages/formdel"); 
        
    });
    
    app.post('/deletar', function(req, res){
        
        var contato = req.body;
        console.log(contato);
        var connection = app.infra.connectionFactoryRDS();
        var contatosDAO = new app.infra.ContatosDAO(connection);
        
        
        
       	contatosDAO.delete(contato, function(err, result){
            res.redirect("/contatos"); 
            
        });
        
    });
    
}