// ja esta carregado var connectionFactory = require('../infra/connectionFactory');

module.exports = function(app){
    
    app.get('/contatos', function(req, res){
        
        console.log("listando");
        var connection = app.infra.connectionFactoryRDS();
        var contatosDAO = new app.infra.ContatosDAO(connection);
        
        contatosDAO.list(function(err, result){
            
            res.format({
                html: function(){
                    res.render("pages/lista", {list:result}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
    
    app.get('/contatos/form', function(req, res){
        
        res.render("pages/form"); 
        
    });
    
    app.post('/contatos', function(req, res){
        
        var contato = req.body;
        console.log(contato);
        var connection = app.infra.connectionFactoryRDS();
//        var s3 = app.infra.connectionFactoryS3();
        var contatosDAO = new app.infra.ContatosDAO(connection);
        
        
        
       	contatosDAO.save(contato, function(err, result){
            console.log(contato);
//            var params = {
//                Bucket: "contatos-agenda",
//                Body: contato.foto
//            };
//            
//            s3.putObject(params, function(err,res){
//                if(err){
//                    console.log("erro");
//                }else{
//                    console.log("Deu certo");
//                }
//            });
            res.redirect("/contatos"); 
            
        });
        
    });
}
